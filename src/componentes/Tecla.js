import React from 'react';



export default function Tecla(props){
    return <button onClick={()=>props.onClick(props.texto)} >{props.texto}</button>
}