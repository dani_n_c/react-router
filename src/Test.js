import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Navbar, Nav } from 'react-bootstrap';

import Home from './Home';
import Previsio from './componentes/Previsio';
import Calculadora from './componentes/Calculadora';
import Pagina3 from './componentes/Pagina3';
import Pagina4 from './componentes/Gente';
import NotFound from './NotFound';

export default () => (
    <BrowserRouter>
        <Container>
            <Row>
                <Col>
                    <Navbar bg="dark" variant="dark">
                        <Navbar.Brand ><Link to="/">Home</Link></Navbar.Brand>
                        <Nav className="mr-auto">
                            
                            <Nav.Link ><Link to="/pagina1">Previsio</Link></Nav.Link>
                            <Nav.Link ><Link to="/calculadora">Calculadora</Link></Nav.Link>
                            <Nav.Link ><Link to="/pagina3">Pagina3</Link></Nav.Link>
                            <Nav.Link ><Link to="/gente">Pagina4</Link></Nav.Link>
                        </Nav>
                      
                    </Navbar>
                   
                    
                </Col>
            </Row>

            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/pagina1" component={Previsio} />
                <Route path="/calculadora" component={Calculadora} />
                <Route path="/pagina3" component={Pagina3} />
                <Route path="/gente" component={Pagina4} />
                <Route component={NotFound} />
            </Switch>

        </Container>
    </BrowserRouter>
);
