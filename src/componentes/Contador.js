import React, { Component } from 'react';
import Dato from  './Dato';

class Contador extends Component {
    constructor(props){
        super(props);

        this.state = {
            display:parseInt(this.props.valorInicial)
        };
        this.displayMas=this.displayMas.bind(this);
        this.displayMenos=this.displayMenos.bind(this);
    }
    displayMenos(){
        if (this.state.display>0){
        this.setState({
            display: this.state.display-1            
        })
    }else{
        window.alert("no introduzcas menos")
    }
    }
      displayMas(){
        if (this.state.display+2<10){
        this.setState({
            display: this.state.display+1            
        })
    }else{
        window.alert("no introduzcas mas")
    }
    }
    render(){
       
        return (
            <React.Fragment>
            <button onClick={this.displayMenos} >Menos</button>
            {/*<h2>{this.state.display}</h2>*/}
            <Dato valor={this.state.display}/>
            <Dato valor={this.state.display+1}/>
            <Dato valor={this.state.display+2}/>
            <button onClick={this.displayMas} >Mas</button>
   
            </React.Fragment>
        );
    }
}
export default Contador;