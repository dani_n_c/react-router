import React, { Component } from 'react';
//import '.css/Gente.css';
import {  Col, Card} from 'react-bootstrap';


class Gente extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            personas: {}

        };

        this.getGente = this.getGente.bind(this);
        this.getGente();

    }
    
    
    
    
    
    getGente(){
        //consultar formato: https://randomuser.me/api/?results=10
    
        const cantidad =10;
        const apiUrl = `https://randomuser.me/api/?results=${cantidad}`;
        
        fetch(apiUrl)
            .then(response => response.json())
            
            .then(personas => this.setState({personas}))
            .catch(error => console.log(error));
    }
   
    
    render() { 
        if (this.state.personas.length===0) {
            return <h1>Cargando datos...</h1>
        }

        let gente=[];
       
        for (var property in this.state.personas) {
            if (this.state.personas.hasOwnProperty(property)) {
                let persona = this.state.personas[property];
                gente.push(<tr key={property}>
                                <td>{property.results}</td>
                                
                            </tr>);
            }
        }

        return ( 
        <>
       <Col xs="6" md="4" lg="3" >
       <Card >
       
            {gente}
       </Card>
       </Col>

    
        </>
        );
    }
}
 
export default Gente;