
import React, { Component } from 'react';
import './css/Mosca.css';
//importes font awesome soy un comentario
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStroopwafel } from '@fortawesome/free-solid-svg-icons';
library.add(faStroopwafel);

/*function Mosca (props){
    return <i className="fas fa-spider mosca"/>
}
*/


export const Mosca = () => (
  <div>
     <FontAwesomeIcon icon="fas fa-spider" />
  </div>
)
export default Mosca;