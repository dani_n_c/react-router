import React from "react";
import ReactDOM from "react-dom";

import Test from "./Test";
import 'bootstrap/dist/css/bootstrap.min.css';
import * as serviceWorker from './serviceWorker';
//import 'font-awesome/css/font-awesome.min.css';

ReactDOM.render(<Test />, document.getElementById("root"));
